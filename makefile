CXX=mpicxx
RM=rm -f
RUN=mpirun -np 4

SRCS=main.cpp Board.cpp Game.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

all: out/life

out/life: $(OBJS)
	$(CXX) -o out/life $(OBJS)

clean:
	$(RM) $(OBJS)

tests: testParallelBocks testParallelLines

testParallelBlocks:
	mpirun -np 9 out/life in/input.txt 1 blocks > block.txt
	mpirun -np 1 out/life in/input.txt 1 seq > seq.txt
	diff seq.txt block.txt

testParallelLines:
	mpirun -np 9 out/life in/input.txt 20 lines > lines.txt
	mpirun -np 1 out/life in/input.txt 1 seq > seq.txt
	diff seq.txt block.txt

run:
	make && $(RUN) out/life in/input.txt 1
