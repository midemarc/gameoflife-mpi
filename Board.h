//
// Created by marc on 28/04/16.
//

#ifndef GAMEOFLIFE_MPI_BOARD_H
#define GAMEOFLIFE_MPI_BOARD_H

#include <vector>
#include <iostream>
#include <istream>
#include <fstream>
#include <iostream>
#include <cstring>


using namespace std;

class Board {
public:
    int width;
    int height;

    Board(Board *board);

    Board(char *filename);

    void set(int x, int y, bool value);
    ~Board();

    int getWidth() {
        return this->width;
    }

    int getHeight() const {
        return height;
    }

    void setWidth(int width) {
        Board::width = width;
    }

    void setHeight(int height) {
        Board::height = height;
    }

    bool **getArray() {
        return this->array;
    }

    void setValue(int x, int y, bool value) {
        this->array[x][y] = value;
    }

    void setArray(bool **array) {
        this->array = array;
    }

    void toFile(ostream &out);

    void print();

    int* toIntArray();
private:
//    int width;
//    int height;
    bool **array;
};


#endif //GAMEOFLIFE_MPI_BOARD_H
