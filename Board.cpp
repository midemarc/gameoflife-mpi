//
// Created by marc on 28/04/16.
//

#include <istream>
#include <fstream>
#include <iostream>
#include <cstring>
#include <stdlib.h>
#include "Board.h"

//using namespace std;

Board::Board(Board *board) {
    this->width = board->width;
    this->height = board->height;
    setArray((bool**)malloc(height));
    for(int i = 0; i < board->height; i++){
        this->array[i] = (bool*)malloc(width);
    }
}

Board::Board(char *filename) {
    ifstream in(filename);

    if (!in.good()) return;
    in >> this->height;
//    cout << "height : " << height << endl;

    if (!in.good()) return;
    in >> this->width;
//    cout << "width : " << this->width << endl;

    int nLine = 0;
    std::string line;
    in.ignore(256, '\n');
    this->array = new bool*[height];
    bool *booleanLine;
    for (in; getline(in, line);) {
//        cout << "line : ";
        booleanLine = new bool[width];
	for (int i = 0; i < width; i++) {
//            cout << line[i];
            if (line[i] != '\n') {
                booleanLine[i] = line[i] == '1';
            }
        }
//        cout << endl;
        this->array[nLine] = booleanLine;
        nLine++;

    }
//    print();
}

void Board::print() {
    for (int y = 0; y < this->height; y++) {
        cout << "line : ";
        for (int x = 0; x < this->width; x++) {
            cout << this->getArray()[y][x];
        }
        cout << endl;
    }
}

void Board::toFile(ostream &out) {
    for (int i = 0; i < this->height; i++) {
        for (int j = 0; j < this->width; j++) {
            if (!out.good()) return;
            out << this->array[i][j];
            if ((j + 1) == this->width) {
                out << endl;
            }
        }
    }
}

Board::~Board() {
    delete this->array;
}



