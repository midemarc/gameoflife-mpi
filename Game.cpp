//
// Created by marc on 28/04/16.
//

#include <fstream>
#include "Game.h"

using namespace std;

Game::Game(Board *board) {
    this->currentBoard = board;
}

void Game::parallelByBlock_distributeBySend(int nbIterations, int nbProcs) {
    int rank;
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    bool** myArray;
    if(rank==0) {
        for (int i = 0; i < nbProcs; i++) {

            int columnsPerProc = this->currentBoard->width / (sqrt(nbProcs));
            int linesPerProc = this->currentBoard->height / (sqrt(nbProcs));
            //Compute boundaries
            pair<int, int> intervalColumn = computeBoundariesWithoutMargins(nbProcs, true, i, columnsPerProc, linesPerProc);
            pair<int, int> intervalLine = computeBoundariesWithoutMargins(nbProcs, false, i, columnsPerProc, linesPerProc);
            //cout << "Proc " << i << " columns " << intervalColumn.first << "-" << intervalColumn.second << " lines " << intervalLine.first << "-" << intervalLine.second << endl;

            //Create subArray

            bool **array = new bool *[linesPerProc];
            for (int j = 0; j < linesPerProc; j++) {
//            array[j] = malloc(columnsPerProc * sizeof(bool));
                array[j] = new bool[columnsPerProc];
                for (int k = 0; k < columnsPerProc; k++) {
                    array[j][k] = this->currentBoard->getArray()[intervalLine.first + j][intervalColumn.first + k];
                    //cout << array[j][k];
                }
                //cout << endl;
            }
            //Non-blocking send
            MPI_Request request;
            MPI_Isend(&array,columnsPerProc*linesPerProc,MPI_BYTE,i,0,MPI_COMM_WORLD,&request);
//        cout << "After : Rank " << i << "columns " << intervalColumn.first << "-" << intervalColumn.second << " lines " << intervalLine.first << "-" << intervalLine.second << endl; //Still useful to debug
        }
        //cout << myArray.s
    }


//    MPI_Bcast(&nbProcs,1,MPI_INT,0,MPI_COMM_WORLD);
}




pair<int,int> Game::computeBoundaries(int nbProcs, bool isColumn, int rank) {
    int columnPerProc = this->currentBoard->width / (sqrt(nbProcs));
    int linesPerProc = this->currentBoard->width / (sqrt(nbProcs));

    if(isColumn) {
        pair<int,int> intervalColumn = computeBoundariesWithoutMargins(nbProcs,true,rank, columnPerProc, linesPerProc);
//        cout << "Before : Rank " << rank << "columns " << intervalColumn.first << "-" << intervalColumn.second << endl; //Still useful to debug
        if(intervalColumn.first > 1) {intervalColumn.first -= 1;}
        if(intervalColumn.second < (this->currentBoard->width-1)) {intervalColumn.second += 1;}
        return intervalColumn;
    } else {
        pair<int,int> intervalLine = computeBoundariesWithoutMargins(nbProcs,false,rank, columnPerProc, linesPerProc);
//        cout << "Before : Rank " << rank << " lines " << intervalLine.first << "-" << intervalLine.second << endl; //Still useful to debug

        if(intervalLine.first > 1) {intervalLine.first -= 1;}
//        cout << "width : " << this->currentBoard->width << endl; //Still useful to debug
        if(intervalLine.second < (this->currentBoard->height-1)) {intervalLine.second += 1;}
        return intervalLine;
    };
};

pair<int,int> Game::computeBoundariesWithoutMargins(int nbProcs, bool isColumn, int rank, int columnPerProc, int linesPerProc) {

    if(isColumn) {
        int columnBegin = (rank * columnPerProc) % nbProcs;
        int columnEnd = columnBegin + (columnPerProc - 1);
//        cout << "Rank:" << rank << " columns " << columnBegin << "-" << columnEnd << endl; //Still useful to debug
        return make_pair(columnBegin, columnEnd);
    } else {
        int lineBegin = (int(rank / sqrt(nbProcs))) * linesPerProc;
        int lineEnd = lineBegin + (linesPerProc - 1);
//        cout << "Rank:" << rank << " lines " << lineBegin << "-" << lineEnd<< endl; //Still useful to debug
        return make_pair(lineBegin, lineEnd);
    }
}

void Game::sequentialPlay(int nbIterations) {


    for (int i = 0; i < nbIterations; i++) {
        this->nextBoard = new Board(currentBoard);
        for (int y = 0; y < this->currentBoard->height; y++) {
            for (int x = 0; x < this->currentBoard->width; x++) {
                this->nextBoard->setValue(y, x, checkState(x, y));
            }
        }
//        this->nextBoard->print();
//        cout << endl;
        this->currentBoard->setArray(this->nextBoard->getArray());
//        this->currentBoard->print();
//        cout << endl;
    }
}

bool Game::checkState(int x, int y) {

    int z = countNeighbours(x, y);
    if (getCurrentState(x, y)) {
        return (z == minNbCellsToSurvive || z == maxNbCellsToSurvive);
    } else {
        return z == nbCellsToBecomeAlive;
    }
}


bool Game::getCurrentState(int x, int y) {
    return this->currentBoard->getArray()[y][x];
}

int Game::countNeighbours(int x, int y) {

    int neighboursCount = 0;
    //cout << " count " << this->currentBoard->getArray()[1][1] << endl;

    int width = this->currentBoard->width;
    int height = this->currentBoard->height;

    for (int j = y - neighbourSearchSize; j <= y + neighbourSearchSize; j++) {
        for (int i = x - neighbourSearchSize; i <= x + neighbourSearchSize; i++) {
            if (!(i < 0 || i >= width || j < 0 || j >= height || (j == y && i == x))) {
                neighboursCount += this->currentBoard->getArray()[j][i];
            }
        }
    }

    return neighboursCount;

}

void Game::parallelPerLinesPlay(vector<vector<bool> > myArray){

    for (int y = 0; y < myArray.size(); y++) {
        for (int x = 0; x < myArray[y].size(); x++) {
            myArray[y][x] = checkState(x, y);
        }
    }

    for (int y = 0; y < myArray.size(); y++) {
//        cout << "line : ";
        for (int x = 0; x < myArray[y].size(); x++) {
            cout << myArray[y][x] << endl;
        }
        cout << endl;
    }
}