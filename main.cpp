#include <iostream>
#include "Board.h"
#include "Game.h"
#include <stdlib.h> //Ne pas enlever (atoi)
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <assert.h>

using namespace std;
#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << endl; \
            exit(EXIT_FAILURE); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

int main(int argc, char *argv[]) {
    int32_t size;
    int32_t rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    Board *b;
    if(rank==0) {
        //Init board
        b = new Board(argv[1]);
        //Conditions to make it easier
//        ASSERT(b->height%size==0, "Height of the matrix was not divible by the number of cores available ; was " << b->height);
//        ASSERT(sqrt(size)==floor(sqrt(size)), "Number of cores should be a square but was " << size);
        Game* game = new Game(b);
        cout << "Rank:" << rank << " init GAME done " << endl;

//    game->sequentialPlay(atoi(argv[2]));
        game->parallelByBlock_distributeBySend(atoi(argv[2]),size);
        cout << "Parallel by block done" << endl;
    }






    MPI_Finalize();
    return 0;
}