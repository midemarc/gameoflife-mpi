//
// Created by marc on 28/04/16.
//

#ifndef GAMEOFLIFE_MPI_GAME_H
#define GAMEOFLIFE_MPI_GAME_H
#include "Board.h"
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <math.h>
#include <mpi.h>
//#include <cstring>

class Game {
public:
    Game(Board* board);
    void sequentialPlay(int nbIterations);
    void parallelByBlock_distributeBySend(int nbIterations, int nbProcs);
    void parallelPerLinesPlay(vector<vector<bool> > myArray);
    std::pair<int,int> computeBoundaries(int nbProcs, bool isColumn, int rank);
    std::pair<int,int> computeBoundariesWithoutMargins(int nbProcs, bool isColumn, int rank, int columnPerProc, int linesPerProc);

private:
    bool checkState(int x, int y);
    Board* currentBoard;
    Board* nextBoard;
    int countNeighbours(int x, int y);
    bool getCurrentState(int x, int y);
    static int const neighbourSearchSize=1;
    static int const minNbCellsToSurvive = 2;
    static int const maxNbCellsToSurvive = 3;
    static int const nbCellsToBecomeAlive = 3;
};

#endif //GAMEOFLIFE_MPI_GAME_H
